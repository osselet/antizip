#!/usr/bin/env python3

"""antizip

This script tries to extract the content of a password protected zip file,
using one or more dictionnaries to guess the password.

To work with AES encrypted zip files, the pyzipper package is required.

REQUIREMENTS

    pip install -r requirements.txt

USAGE

  antizip.py [-h] [-w WORKERS] -p [PASSWORD_FILES [PASSWORD_FILES ...]]
                    zip_file

  positional arguments:
    zip_file              path to the target zip file

  optional arguments:
    -h, --help            show this help message and exit
    -w WORKERS, --workers WORKERS
                          the number of worker processes to use (max = cpu)
    -p [PASSWORD_FILES [PASSWORD_FILES ...]], --password-files [PASSWORD_FILES [PASSWORD_FILES ...]]
                          one or more passwords file

EXAMPLES

    brute force the target.zip file using the 1000-common-passwords-en.txt
    dictionary

        python3 antizip.py target.zip -p 1000-common-passwords-en.txt

    using several dictionaries sequentially

        python3 antizip.py target.zip -p 1000-common-passwords-en.txt 1000-common-passwords-ru.txt

    spawn 4 worker processes (by default it spawns one process per cpu)

        python3 antizip.py target.zip -p 1000-common-passwords-en.txt -w 4
"""


import argparse
from functools import partial
from multiprocessing import Pool
import os
import sys
import zipfile

import pyzipper


OUTPUT_DIR = './.antizip'


def _get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('zip_file', help='path to the target zip file')
    parser.add_argument(
        '-w', '--workers', required=False, type=int, default=os.cpu_count(),
        help='the number of worker processes to use (max = cpu)',
    )
    parser.add_argument(
        '-p', '--password-files', required=True, nargs='*',
        help='one or more passwords file'
    )
    return parser.parse_args()


def try_extraction(password, zip_file):
    with pyzipper.AESZipFile(zip_file) as z_file:
        try:
            z_file.extractall(path=OUTPUT_DIR, pwd=password.encode('UTF-8'))
        except RuntimeError:
            return ''

        # Bad HMAC check happen when z_file.extractall is called too much time
        # in a row
        except pyzipper.zipfile.BadZipFile as e:
            print('-- exception raised: {} -- password {} is skipped'.format(e, password))
            return ''
        else:
            return password


def _display_result(password):
    if password:
        print('|')
        print('+--------[{}] !'.format(password))
    else:
        print('|')
        print('+--------[] ?')
    print('')


def main():
    """
    Get things done.
    """
    args = _get_args()

    try:
        os.mkdir(OUTPUT_DIR)
    except FileExistsError:
        pass

    guessed_password = ''
    with Pool(processes=args.workers) as pool:

        for password_file in args.password_files:
            with open(password_file) as passwords:
                print('using {} ...'.format(password_file))
                for guess in pool.imap_unordered(
                    partial(try_extraction, zip_file=args.zip_file),
                    (line.strip('\n') for line in passwords),
                    1
                ):

                    # if the truth has been revealed...
                    if guess:
                        guessed_password = guess
                        break

            _display_result(guessed_password)
            # ...there is no need to continue guessing
            if guessed_password:
                break

    # if empty, remove the output directory
    if not guessed_password and os.listdir(OUTPUT_DIR):
        os.rmdir(OUTPUT_DIR)


if __name__ == '__main__':
    main()
    sys.exit(0)
